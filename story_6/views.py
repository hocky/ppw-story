from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from .models import Activity, Person
from .forms import ActivityForm, PersonForm
from django.contrib import messages
import copy

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    context = copy.copy(original)
    context["description"] = "List of activities"
    context["title"] = "Fun! Fun! Activities!"
    context["activities"] = Activity.objects.all()
    form = PersonForm(request.POST or None)
    if(form.is_valid()):
        name = form.cleaned_data['name']
        messages.success(request, f'Successfully added {name}')
        form.save()
        return redirect('story_6:activity')

    context["form"] = form
    return render(request, "activity.html", context)

def add_activity(request):
    context = copy.copy(original)
    context["description"] = "Add more activities here!"
    context["title"] = "Fun! Fun! Activities!"

    form = ActivityForm(request.POST or None)
    if(form.is_valid()):
        name = form.cleaned_data['name']
        messages.success(request, f'Successfully added {name} activity')
        form.save()
        return redirect('story_6:activity')

    context["form"] = form
    return render(request, "add_activity.html", context)


def delete_activity(request, id):
    activity = get_object_or_404(Activity, id=id)

    messages.success(request, f'Successfullydeleted {activity}')
    activity.delete()

    return redirect('story_6:activity')

def delete_person(request, id):
    person = get_object_or_404(Person, id=id)

    messages.success(request, f'Successfully deleted {person}')
    person.delete()

    return redirect('story_6:activity')