from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from .models import Activity, Person


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

class MainTestCase(TestCase):
    def setUp(self):
        Activity.objects.create(name="Kegiatan Asyik", category="🎨 Art")
        Activity.objects.create(name="Kegiatan Asyik Juga", category="🎨 Art")
        Person.objects.create(name="Boba", activity=Activity.objects.get(name="Kegiatan Asyik"))
        Person.objects.create(name="Bobi", activity=Activity.objects.get(name="Kegiatan Asyik Juga"))
    def test_00_make_person(self):
        cnt = Person.objects.all().count()
        Person.objects.create(name='Izuri', activity=Activity.objects.get(name="Kegiatan Asyik"))
        self.assertEqual(Person.objects.all().count(), cnt+1)
    def test_01_make_activity_from_page(self):
        cnt = Activity.objects.all().count()
        response = self.client.post('/activity/add/', data={'name':'Kegiatan', 'category' : "🎮 Game"})
        self.assertEqual(Activity.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 302)
        
    def test_02_make_person_from_page(self):
        cnt = Person.objects.all().count()
        response = self.client.post('/activity/', data={'name':'Boni', 'activity' : '1'})
        self.assertEqual(Person.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 302)

    def test_03_delete_person_from_page(self):
        cnt = Person.objects.all().count()
        response = self.client.get(f'/activity/delete-person/{Person.objects.all()[0].id}/')
        self.assertEqual(Person.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)

    def test_04_delete_activity_from_page(self):
        cnt = Activity.objects.all().count()
        response = self.client.get(f'/activity/delete/{Activity.objects.all()[0].id}/')
        self.assertEqual(Activity.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)

    def test_05_schedule_url_status_200(self):
        response = self.client.get('/activity/')
        self.assertEqual(response.status_code, 200)

    def test_06_add_course_url_status_200(self):
        response = self.client.get('/activity/add/')
        self.assertEqual(response.status_code, 200)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_schedule_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/activity/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_add_course_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/activity/add/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
