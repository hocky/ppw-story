from django.db import models

class Activity(models.Model):
    name = models.CharField(max_length = 50, default = "Fun activity")
    category = models.CharField(max_length = 20, choices=[(x, x) for x in ["🎮 Game", "🎨 Art", "📢 Talk"]], default = "🎮 Game")
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Activities"


class Person(models.Model):
    name = models.CharField(max_length = 50)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "People"
