from django.forms import ModelForm
from .models import Person, Activity
from crispy_forms.helper import FormHelper

class ActivityForm(ModelForm):
    class Meta():
        model = Activity
        fields = '__all__'

class PersonForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.fields['name'].label = ''

    class Meta():
        model = Person
        fields = '__all__'