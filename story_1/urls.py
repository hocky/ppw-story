from django.urls import path
from . import views

app_name = 'story_1'

urlpatterns = [
    # views.<method in views> here
    path('', views.index, name='profile'),
]