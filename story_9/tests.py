from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.contrib.auth.models import User
from .models import Profile

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

class MainTestCase(TestCase):
    def test_login_page_200(self):
        response = self.client.get('/game/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Dive", content)

        
    def test_signup_page_400(self):
        response = self.client.post('/game/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 400)

    def test_signup_page_200(self):
        response = self.client.get('/game/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Sign", content)
    
    def test_login_logout_api(self):
        response = self.client.get('/game/loginapi/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("400", content)
        self.assertEqual(response['content-type'], 'application/json')

    def test_signup_api(self):
        response = self.client.get('/game/signupapi/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("400", content)
        self.assertEqual(response['content-type'], 'application/json')
    
    def test_signup_profile_logout_login_logout_api(self):
        
        response = self.client.post('/game/signupapi/',data={
            'username':'hocky', 'password1':"Zd3b75kkQuFvDxq", 'password2':"ZdQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Failed", content)
        self.assertEqual(response['content-type'], 'application/json')


        response = self.client.post('/game/signupapi/',data={
            'username':'hocky', 'password1':"Zd3b75kkQuFvDxq", 'password2':"Zd3b75kkQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Successful", content)
        self.assertEqual(response['content-type'], 'application/json')

        self.assertEqual(Profile.objects.all().count(), 1)
        self.assertEqual(Profile.objects.all()[0].__str__(), 'hocky')

        response = self.client.get('/game/id/hocky/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("500", content)
        self.assertIn("hocky", content)

        
        response = self.client.get('/game/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("hocky", content)
        self.assertIn("Leaderboard", content)

        response = self.client.get('/game/claim/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/game/id/hocky/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("hocky", content)
        
        response = self.client.post('/game/id/hocky/', data={
            "full_name":"Hocky Yudhiono",
            "photo":"https://media.tenor.com/images/fbe9e47765789e2b6379618d5fc470e5/tenor.gif"
        })
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/game/id/hocky/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("hocky", content)
        self.assertIn("Hocky Yudhiono", content)

        response = self.client.get('/game/signup/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/game/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/game/logout/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("redirected", content)
        self.assertIn("Soon", content)

        response = self.client.post('/game/login/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 400)

        response = self.client.post('/game/loginapi/',data={
            'username':'hoky', 'password':"Zd3b75kkvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("No account", content)
        self.assertEqual(response['content-type'], 'application/json')

        response = self.client.post('/game/loginapi/',data={
            'username':'hocky', 'password':"Zd3b75kkvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("wrong password", content)
        self.assertEqual(response['content-type'], 'application/json')

        response = self.client.post('/game/loginapi/',data={
            'username':'hocky', 'password':"Zd3b75kkQuFvDxq"
        }, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Successful", content)
        self.assertEqual(response['content-type'], 'application/json')

class MainFunctionalTestCase(FunctionalTestCase):

    def test_game_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/game/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_login_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/game/login/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
    
    def test_signup_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/game/signup/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
