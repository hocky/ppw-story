import json

from django.db import models

# Importing the user model django provided
from django.contrib.auth.models import User;

# Hello Hocky please read this in the future if you need help

# In many cases when there is a modification in a model’s instance we need execute some action. Django provides us an elegant way to handle these situations. The signals are utilities that allow us to associate events with actions. We can develop a function that will run when a signal calls it.

# Reference of usage:
# - https://medium.com/@ksarthak4ever/django-signals-b20a4152a27b
# - https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length = 128, default = "", blank = True)
    photo = models.URLField(default = "https://i.pinimg.com/originals/b6/57/c3/b657c3b864b96fe5eefc60bd8841cda4.gif")
    score = models.IntegerField(default=500)
    last_claimed = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.user.__str__()

@receiver(post_save, sender=User)
def save_profile(sender, instance, created, **kwargs):
    if(created):
        Profile.objects.create(user=instance)
    instance.profile.save()
    