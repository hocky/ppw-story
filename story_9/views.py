from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import User, Profile
from .forms import ProfileForm
from random import randint
import copy

# Reference:
# https://docs.djangoproject.com/en/3.1/topics/auth/default/#django.contrib.auth.views.LoginView
# https://docs.djangoproject.com/en/3.1/topics/auth/default/
# https://github.com/django/django/blob/master/django/contrib/auth/forms.py

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    # print(request.session)
    # print(request)
    # print(type(request))
    # try:
    #     print(request.user)
    #     print(type(request.user))
    # except:
    #     print("Not available")
    context = copy.copy(original)
    context["description"] = f"I've won.. But at what cost"
    context["title"] = f"Compete?"
    leaderboard = []
    for user in User.objects.all():
        currentProfile = get_object_or_404(Profile, user=user)
        name = (currentProfile.full_name + " Scary Anonymous").strip().split()
        first_name = name[0].title()
        second_name = name[1].title()
        leaderboard.append((currentProfile.score, first_name, second_name, user.username))
    leaderboard = sorted(leaderboard, reverse=True)
    if(len(leaderboard) > 10): leaderboard = leaderboard[:10]
    context["leaderboard"] = leaderboard
    return render(request, "game.html", context)

def profile(request, username):

    if(request.method == "POST"):
        if(request.user.is_authenticated and username == request.user.username):
            form = ProfileForm(request.POST, instance=get_object_or_404(Profile, user=request.user))
            if(form.is_valid()):
                form.save()

        return redirect('story_9:profile', username=username)

    context = copy.copy(original)
    context["description"] = f"{username}'s Profile"
    context["title"] = f"{username}'s Profile"
    context["other_user"] = get_object_or_404(User, username=username)
    context["other_profile"] = get_object_or_404(Profile, user=context["other_user"])
    if(request.user.is_authenticated and username == request.user.username):
        context["form"] = ProfileForm(instance = context["other_profile"])

    return render(request, "profile.html", context)

def loginview(request):
    if(request.method != "GET"):
        return HttpResponseBadRequest("You may not do that!")

    if(request.user.is_authenticated):
        return redirect('story_9:index')

    context = copy.copy(original)
    context["description"] = f"Loggy loggy log log log in"
    context["title"] = f"Your Ordinary Log In Page"
    
    context["form"] = AuthenticationForm()
    return render(request, "login.html", context)

def loginapi(request):
    if(request.is_ajax and request.method == "POST"):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        # print(user)
        # print(type(user))
        if user is not None:
            login(request, user)
            return JsonResponse({"status":200, "message":"Successful"})
        else:
            try:
                user = User.objects.get(username=username)
                return JsonResponse({"status":400, "message":"You entered the wrong password ☹️"})
            except:
                return JsonResponse({"status":400, "message":"No account with this handle is available 👎"})
    return JsonResponse({"status":400, "message":"Something's wrong 😵"})

def signupview(request):
    if(request.method != "GET"):
        return HttpResponseBadRequest("You may not do that!")

    if(request.user.is_authenticated):
        return redirect('story_9:index')

    context = copy.copy(original)
    context["description"] = f"Signy signy sign sign sign in"
    context["title"] = f"Your Casual Sign In Page"
    
    context["form"] = UserCreationForm()

    # print(context["form"])
    return render(request, "signup.html", context)

# def usernameapi(request, username):
#     if(request.is_ajax and request.method == "GET"):
#         try:
#             user = User.objects.get(username=username)
#             return JsonResponse({"status":True})
#         except:
#             return JsonResponse({"status":False})
            
#     return JsonResponse({"status":400, "message":"Something's wrong 😵"})

def signupapi(request):

    if(request.is_ajax and request.method == "POST"):
        # print(request.POST)
        form = UserCreationForm(request.POST)
        if(form.is_valid()):
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            login(request, new_user)
            return JsonResponse({"status":200, "message":"Signup Successful ✔️", "username":form.cleaned_data['username']})
        else:
            # print("Verdict__________________________________")
            form = form.__str__()
            # print(form)
            return JsonResponse({"status":400, "message":"Signup Failed 😵", "htmlform":form})

    return JsonResponse({"status":400, "message":"Something's wrong 😵"})

def claim(request):
    profile = get_object_or_404(Profile, user=request.user)
    profile.score += randint(50, 250)
    profile.save()
    return JsonResponse({'score' : profile.score})