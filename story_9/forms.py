from django.forms import ModelForm, DateTimeInput, CheckboxInput
from .models import *

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user', 'score']
        labels = {
            'full_name': '',
            'photo': ''
        }
        # widgets = {
        #     'semester_parity': CheckboxInput(attrs={'data-toggle': 'toggle', 'data-on': "Odd", "data-off": "Even", 'data-onstyle':'primary', 'data-offstyle':'warning'})
        # }