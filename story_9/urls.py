from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'story_9'

urlpatterns = [
    path('', views.index, name='index'),
    path('id/<str:username>/', views.profile, name='profile'),
    path('login/', views.loginview, name='login'),
    path('loginapi/', views.loginapi, name='loginapi'),
    path('signupapi/', views.signupapi, name='signupapi'),
    # path('login/', auth_views.LoginView.as_view(template_name="login_django.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="logout_django.html"), name='logout'),
    path('signup/', views.signupview, name='signup'),
    path('claim/', views.claim, name='claim'),
]