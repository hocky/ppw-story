from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from unittest import mock

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_random_url_goes_to_404(self):
        response = self.client.get('/adsfasdfsf/')
        self.assertEqual(response.status_code, 200)
    def test_400(self):
        response = self.client.get('error/400/sdfasf')
        self.assertEqual(response.status_code, 200)
    def test_403(self):
        response = self.client.post('error/403/asdds')
        self.assertEqual(response.status_code, 200)
    def test_404(self):
        response = self.client.get('error/404/safsf')
        self.assertEqual(response.status_code, 200)
    def test_500(self):
        response = self.client.get('error/500/')
        self.assertEqual(response.status_code, 200)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_url_does_not_exists(self):
        self.selenium.get(f'{self.live_server_url}/adsfsfsdfa/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
    def test_400_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/error/400/safdsafsf/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
    def test_403_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/error/403/sadsfafsf/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
    def test_404_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/error/404/safsdff/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
    def test_500_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/error/500/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())