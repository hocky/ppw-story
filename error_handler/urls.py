from django.urls import path
from . import views

app_name = 'error'

urlpatterns = [
    # views.<method in views> here
    path('400/<str:exception>/', views.error_400, name='400'),
    path('403/<str:exception>/', views.error_403, name='403'),
    path('404/<str:exception>/', views.error_404, name='404'),
    path('500/', views.error_500, name='500'),
]