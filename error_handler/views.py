from django.shortcuts import render
import copy

original = {
    "description" : "Error(?)",
    "title" : "Something's wrong!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
    "glitchtext" : "Ő̵͙̤̖̝̺̅̅́̕o̸̧̘̳̝͎͑̀̇̈́̉̎̆͒́̎͘f̷̛͙̗͔̖̬͔̼̞̦̦̝̦̬̀̄̊͗̂ͅ",
}

# bad request error
def error_400(request, exception):
    context = copy.copy(original)
    context["description"] = "Bad request!"
    context["title"] = "Baddy Baddy Bad"
    context["exception"] = exception
    return render(request, 'error.html', context)

# permission denied
def error_403(request, exception):
    context = copy.copy(original)
    context["description"] = "Permission denied!"
    context["title"] = "Noo.. Noo.. No"
    context["exception"] = exception
    return render(request, 'error.html', context)

# page not found
def error_404(request, exception):
    context = copy.copy(original)
    context["description"] = "Page not found!"
    context["title"] = "Where is the page?"
    context["exception"] = exception
    return render(request, 'error.html', context)

# server error
def error_500(request):
    context = copy.copy(original)
    context["description"] = "Server error!"
    context["title"] = "My bad(?)"
    return render(request, 'error.html', context)

    