from django.shortcuts import render
import copy

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    context = copy.copy(original)
    context["showfooter"] = False
    context["stickynav"] = False
    context["ismain"] = True
    return render(request, 'index.html', context)
    
def gallery(request):
    context = copy.copy(original)
    context["description"] = "Hocky Yudhiono's gallery"
    context["title"] = "Gallery of Memory"
    return render(request, 'gallery.html', context)

