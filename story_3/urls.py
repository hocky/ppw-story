from django.urls import path
from . import views

app_name = 'story_3'

urlpatterns = [
    # views.<method in views> here
    path('', views.index, name='home'),
    path('gallery/', views.gallery, name='gallery'),
]