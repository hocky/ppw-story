from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from .models import Course, Assignment
from .forms import CourseForm, AssignmentForm
from django.contrib import messages
import copy

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def schedule(request):
    context = copy.copy(original)
    context["description"] = "Add your daily schedule here!"
    context["title"] = "Coursey Coursey"
    context["courses"] = Course.objects.all()
    return render(request, "schedule.html", context)


def add_course(request):
    context = copy.copy(original)
    context["description"] = "Add your daily schedule here!"
    context["title"] = "Coursey Coursey"

    form = CourseForm(request.POST or None)
    if(form.is_valid()):
        # print(request.POST)
        course_name = form.cleaned_data['name']
        messages.success(request, f'Successfully added {course_name} Course')
        form.save()
        return redirect('story_5:schedule')

    context["form"] = form
    return render(request, "add_course.html", context)

    
def delete_course(request, id):

    context = copy.copy(original)
    context["course"] = get_object_or_404(Course,id=id)
    if request.method == 'POST':
        messages.success(request, f'Successfully deleted {context["course"].name} Course')
        context["course"].delete()
        return redirect('story_5:schedule')

    context["title"] = f'Delete {context["course"].name}'
    context["description"] = f'Delete {context["course"].name}'
    context["toptitle1"] = "Are you sure you want to delete"
    context["toptitle2"] = f'{context["course"].name}?'
    context["show_delete"] = True
    return render(request, "detail_course.html", context)

def delete_assignment(request, id):

    assignment = get_object_or_404(Assignment, id=id)
    if request.method == 'POST':
        messages.success(request, f'Successfully deleted {assignment} assignment')
        assignment.delete()

    course = request.POST.get('course')
    return redirect('story_5:detail_course', id=course)
    
def detail_course(request, id):
    context = copy.copy(original)
    context["course"] = get_object_or_404(Course,id=id)
    context["title"] = f'{context["course"].name}'
    context["description"] = f'{context["course"].name} course detail'
    context["toptitle1"] = "Course"
    context["toptitle2"] = "Detail"
    context["assignments"] = Assignment.objects.filter(course=context["course"])

    form = AssignmentForm(request.POST or None)

    if(form.is_valid()):
        assignment_name = form.cleaned_data['name']
        messages.success(request, f'Successfully added {assignment_name} Assignment')
        form.save()
        form = AssignmentForm()

    context["form"] = form

    return render(request, "detail_course.html", context)