from django.forms import ModelForm, DateTimeInput, CheckboxInput
from .models import *


class InstructorForm(ModelForm):
    class Meta:
        model = Instructor
        fields = '__all__'


class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'
        labels = {
            'name': 'Course Name',
            'semester_parity': 'Semester',
            'semester_year': 'Year'
        }
        widgets = {
            'semester_parity': CheckboxInput(attrs={'data-toggle': 'toggle', 'data-on': "Odd", "data-off": "Even", 'data-onstyle':'primary', 'data-offstyle':'warning'})
        }

class AssignmentForm(ModelForm):

    class Meta:
        model = Assignment
        fields = '__all__'
        labels = {
            'name': 'Assignment Name'
        }
        widgets = {
            'deadline': DateTimeInput(attrs={'class': 'datetimepicker'}),
        }
