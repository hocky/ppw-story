from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
import datetime

def get_year_choice():
    return [(year, f'{year}/{year+1}') for year in range(datetime.date.today().year-2, datetime.date.today().year+2)];

def get_parity():
    return datetime.date.today().month > 6

class Instructor(models.Model):
    name = models.CharField(max_length=100)
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    gender = models.BooleanField(default=True)
    email = models.EmailField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["rating"]

class Course(models.Model):
    name = models.CharField(max_length=50)
    instructors = models.ManyToManyField(Instructor)
    credit = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(20)], default=3)
    description = models.TextField(default="A fun programming course any beginners can have!")
    semester_parity = models.BooleanField(default=get_parity())
    semester_year = models.IntegerField(choices=get_year_choice(), default=datetime.date.today().year)
    room = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.name} - {self.semester_year}/{self.semester_year+1} {"Gasal" if self.semester_parity else "Genap"} - {self.credit} credit(s)'

class Assignment(models.Model):
    name = models.CharField(max_length=30)
    deadline = models.DateTimeField()
    course = models.ForeignKey(Course, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["deadline"]