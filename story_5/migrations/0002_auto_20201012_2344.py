# Generated by Django 3.1.2 on 2020-10-12 16:44

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story_5', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('rating', models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(10)])),
                ('gender', models.BooleanField(default=True)),
                ('email', models.EmailField(max_length=254)),
            ],
            options={
                'ordering': ['rating'],
            },
        ),
        migrations.RemoveField(
            model_name='course',
            name='credit',
        ),
        migrations.RemoveField(
            model_name='course',
            name='instructor',
        ),
        migrations.AlterField(
            model_name='course',
            name='semester_parity',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='course',
            name='instructors',
            field=models.ManyToManyField(to='story_5.Instructor'),
        ),
    ]
