from django.urls import path
from . import views

app_name = 'story_5'

urlpatterns = [
    # views.<method in views> here
    path('', views.schedule, name='schedule'),
    path('add/', views.add_course, name='add_course'),
    path('delete/<int:id>/', views.delete_course, name='delete_course'),
    path('deleteasg/<int:id>/', views.delete_assignment, name='delete_assignment'),
    path('detail/<int:id>/', views.detail_course, name='detail_course')
]