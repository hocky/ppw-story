from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from .models import Instructor, Course, Assignment
import datetime
from django.utils import timezone


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def setUp(self):
        Instructor.objects.create(name="Bino", rating = 5, gender=False, email="cobasda@gmail.com")
        course = Course.objects.create(name = "Kelas2", credit = 4, description="Tests",semester_parity=True, semester_year = datetime.date.today().year, room = "AS31")
        course.instructors.add(Instructor.objects.get(name="Bino"))
        Assignment.objects.create(name = "Ayam", deadline = datetime.datetime.now(tz=timezone.utc), course = Course.objects.get(name="Kelas2"))

    def test_00_schedule_url_status_200(self):
        response = self.client.get('/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_00_add_course_url_status_200(self):
        response = self.client.get('/schedule/add/')
        self.assertEqual(response.status_code, 200)

    def test_01_make_instructor(self):
        cnt = Instructor.objects.all().count()
        Instructor.objects.create(name="Izuri", rating = 4, gender=True, email="coba@gmail.com")
        self.assertEqual(Instructor.objects.all().count(), cnt+1)
        self.assertIn("Izuri", str(Instructor.objects.get(name = "Izuri")))
    
    def test_02_make_course(self):
        cnt = Course.objects.all().count()
        Course.objects.create(name = "Kelas", credit = 4, description="Tests", semester_parity=True, semester_year = datetime.date.today().year, room = "AS31")
        self.assertEqual(Course.objects.all().count(), cnt+1)
        self.assertIn("Kelas", str(Course.objects.get(name = "Kelas")))

    def test_03_make_assignment(self):
        cnt = Assignment.objects.all().count()
        Assignment.objects.create(name="Tugas", deadline=datetime.datetime.now(tz=timezone.utc), course = Course.objects.get(name="Kelas2"))
        
        self.assertEqual(Assignment.objects.all().count(), cnt+1)
        self.assertIn("Tugas", str(Assignment.objects.get(name = "Tugas")))

    def test_04_add_course_success(self):
        cnt = Course.objects.all().count()
        response = self.client.post('/schedule/add/', data = {'name' : 'Coba Course', 'instructors' : [Instructor.objects.get(name="Bino").id], 'credit' : 3, 'description' : 'test', 'semester_year' : datetime.date.today().year, 'semester_parity' : False, 'room' : 'Test' })
        # print(response)
        self.assertEqual(Course.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 302)

    def test_05_add_assignment_from_page(self):
        cnt = Assignment.objects.all().count()
        self.assertGreater(Course.objects.all().count(), 0)
        course = Course.objects.all()[0]
        response = self.client.post(f'/schedule/detail/{course.id}/', data = {'name' : "Tugas Baru", 'deadline' : datetime.datetime.now(tz=timezone.utc), 'course' : f'{course.id}'})
        self.assertEqual(Assignment.objects.all().count(), cnt+1)
        self.assertEqual(response.status_code, 200)

    def test_06_delete_assignment_from_page(self):
        cnt = Assignment.objects.all().count()
        self.assertGreater(cnt, 0)
        assignment = Assignment.objects.all()[0]
        courseid = assignment.course.id
        response = self.client.post(f'/schedule/deleteasg/{assignment.id}/', data = {'course' : courseid})
        self.assertEqual(Assignment.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)

    def test_07_delete_course_from_page(self):
        cnt = Course.objects.all().count()
        self.assertGreater(cnt, 0)
        course = Course.objects.all()[0]
        response = self.client.get(f'/schedule/delete/{course.id}/')
        response = self.client.post(f'/schedule/delete/{course.id}/')
        self.assertEqual(Course.objects.all().count(), cnt-1)
        self.assertEqual(response.status_code, 302)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_schedule_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/schedule/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

    def test_add_course_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/schedule/add/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())