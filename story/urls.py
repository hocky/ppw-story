"""story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import os

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('story-1/', include('story_1.urls')),

    # path('story-x/', include('story_x.urls')), do this for each new story, and update the landing page with the latest story

    path('error/', include('error_handler.urls')),
    path('profile/', include('story_1.urls')),
    path('', include('story_3.urls')),
    path('schedule/', include('story_5.urls')),
    path('activity/', include('story_6.urls')),
    path('flexes/', include('story_7.urls')),
    path('books/', include('story_8.urls')),
    path('game/', include('story_9.urls')),
    path('copypasta/', include('copypasta.urls')),
]

handler400 = 'error_handler.views.error_400'
handler403 = 'error_handler.views.error_403'
handler404 = 'error_handler.views.error_404'
handler500 = 'error_handler.views.error_500'