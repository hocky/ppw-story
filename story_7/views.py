from django.shortcuts import render
import copy

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    context = copy.copy(original)
    context["description"] = "Hocky Learns JQuery's Accordion"
    context["title"] = "More About Me"
    return render(request, "flexes.html", context)
