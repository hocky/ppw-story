from django.urls import path
from . import views

app_name = 'story_7'

urlpatterns = [
    # views.<method in views> here
    path('', views.index, name='accordion'),
]