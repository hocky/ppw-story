from django.shortcuts import render
import copy

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    context = copy.copy(original)
    original["description"] = "Ayo menjadi degen"
    original["title"] = "Copypasta Generator"
    return render(request, 'copypasta.html', context)
    