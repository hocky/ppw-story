from django.apps import AppConfig


class CopypastaConfig(AppConfig):
    name = 'copypasta'
