from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_books_url_status_200(self):
        response = self.client.get('/books/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("book", content)
    
    def test_api_call(self):
        response = self.client.get('/books/search/asds/')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        # print(content)
        # print(type(response))
        # print(response.json())
        self.assertIn("items", content)
        self.assertEqual(response['content-type'], 'application/json')        

class MainFunctionalTestCase(FunctionalTestCase):

    def test_books_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/books/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
