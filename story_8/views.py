from django.shortcuts import render, redirect
from django.http import JsonResponse
import copy, requests

original = {
    "description" : "Hocky Yudhiono's personal website",
    "title" : "Hey! It's Hocky!",
    "showfooter" : True,
    "stickynav" : True,
    "ismain" : False,
}

def index(request):
    context = copy.copy(original)
    context["description"] = "Hocky Learns AJAX"
    context["title"] = "Books Search"
    return render(request, "books.html", context)
    
def search(request, query):
    APILink = "https://www.googleapis.com/books/v1/volumes?q=" + query
    # print(APILink)
    data = requests.get(APILink).json()
    # print(data)
    # print(type(data))
    # print(JsonResponse(data))
    # print(type (JsonResponse(data)))
    return JsonResponse(data)