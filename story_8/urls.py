from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    # views.<method in views> here
    path('', views.index, name='books'),
    path('search/<str:query>/', views.search, name='search'),
]