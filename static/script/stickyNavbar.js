// When the user scrolls the page, execute myFunction
window.onscroll = function () {
  myFunction();
};

// Get the header
const header = document.getElementById("main-navbar");
const content = document.getElementById("first-section");
console.log(content)

// Get the offset position of the navbar
const sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    content.classList.add("extra-padding");
  } else {
    header.classList.remove("sticky");
    content.classList.remove("extra-padding")
  }
}