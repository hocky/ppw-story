window.ityped.init(document.querySelector(".ityped"), {
  
  /**
   * @param {Array} strings An array with the strings that will be animated 
   */
   strings: ['Mari', 'Hocky'],
  
   typeSpeed:  100,
   startDelay:  500,
   backDelay:  700,
   disableBackTyping: true,
});