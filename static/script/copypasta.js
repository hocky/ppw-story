$(document).ready(function () {

  all = ["konten", "konten-objek", "konten-objek-2", "konten-desc", "predikat"];
  const generateButton = document.getElementById("generator");
  const highlight = document.getElementById("id_highlight")
  
  function getLongName(str) {
    let ret = ""
    for(let i = 0;i < str.length;i++){
      let len = Math.floor(Math.random()*5+4);
      for(let j = 0;j < len;j++) ret += str[i];
    }
    return ret;
  }
  generateButton.addEventListener("click", ()=>{
    for(let i = 0;i < all.length;++i){
      console.log(all[i])
      const content = document.getElementById(all[i]).value;
      const span_content = document.getElementsByClassName(all[i]);
      console.log(span_content)
      for(let j = 0;j < span_content.length;j++){
        span_content[j].innerHTML = content;
      }
      console.log(content);
    }
    document.getElementsByClassName("konten-long")[0].innerHTML = getLongName(document.getElementById("konten").value)
  })
  const span_elements = document.getElementsByTagName("span");
  $('#id_highlight').change(function() {
    for(let i = 0;i < span_elements.length;i++){
      if($(this).prop('checked')) span_elements[i].classList.add("bold-font")
      else span_elements[i].classList.remove("bold-font")
    }
  })
});
