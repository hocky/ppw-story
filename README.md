# Personal Website! 😳

[![pipeline status](https://gitlab.com/hocky/ppw-story/badges/master/pipeline.svg)](https://gitlab.com/hocky/ppw-story/-/commits/master) [![coverage report](https://gitlab.com/hocky/ppw-story/badges/master/coverage.svg)](https://gitlab.com/hocky/ppw-story/-/commits/master)

This is my website for [CSGE602022] Web Design and Programming 2020/2021. This has been deployed and available [here](https://hocky.id/).
Kudos to [Muhammad Ariq Basyar](https://gitlab.cs.ui.ac.id/ariqbasyar) as the main Lecture Assistant that guided me during the course.